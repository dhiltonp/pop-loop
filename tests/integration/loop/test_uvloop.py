import asyncio
import os

import pytest


@pytest.mark.skipif(os.name == "nt", reason="uvloop does not support Windows")
def test_backend(hub):
    hub.loop.init.create("uv")
    assert "uvloop.Loop" in repr(hub.pop.loop.CURRENT_LOOP)
    assert hub.loop.init.backend() == "asyncio"
    assert hub.loop.init.BACKEND == "uv"
    hub.pop.Loop.run_until_complete(asyncio.sleep(0))
